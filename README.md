# Buybayux
This gem incorporates the BuyBay UX in frontend applications.

## Usage


## Installation
Add this line to your application's Gemfile:
```ruby
gem 'buybayux', path: '../buybayux'
```

(make sure that you place the buybayux repo one directory up, or change the path)

And then execute:
```bash
$ bundle
```
Furthermore, remove your applications layout by renaming or removing
```
app/views/layouts/application.html.erb
```

In
```
app/assets/stylesheets/application.css
```
place this:
```ruby
 *= require buybayux/buybayux
```

In
```
app/assets/javascripts/application.js
```
place this:
```ruby
//= require buybayux/buybayux
```

The application provides two layouts. The default is one with the menu on the left. There is also a clean layout.
To use that:
```ruby
render layout: 'buybay_no_menu'
```

## Configuration
The left hand side menu can be configured. There are several ways of doing this, you can use an initializer for a static menu, or set it in a controller:

```ruby
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_navigation
  def set_navigation
    Buybayux.configure do |config|
      config.sidebar = [{:name=>"Sales", :icon=>"fas fa-hand-point-right", :sub_menu=>
        [
          {:name=>"Auctions", :url=>"/auctions"},
          {:name=>"Marketplace offer", :url=>"/marketplace_offer"},
          {:name=>"Customer sale item", :url=>"/marketplace_offer"},
          {:name=>"Customer sale", :url=>"/marketplace_offer"}
        ]
      },
      {:name=>"Test", :icon=>"fas fa-box-open", :url=>"/auctions"},
      {:name=>"Products", :icon=>"fas fa-box-open", :sub_menu=>
        [
          {:name=>"Test", :url=>"/auctions"},
          {:name=>"Marketplace offer", :url=>"/marketplace_offer"},
      ]},
      {:name=>"Finance", :icon=>"fas fa-chart-bar", :sub_menu=>
        [
          {:name=>"Auctions", :url=>"/auctions"},
          {:name=>"Marketplace offer", :url=>"/marketplace_offer"},
          {:name=>"Customer sale item", :url=>"/marketplace_offer"},
          {:name=>"Customer sale", :url=>"/marketplace_offer"}
      ]}

    ]
    end
  end
end
```

config.sidebar is an array with a structured hash, it describes the icons, names and target url's of the menu items. Javascript takes care of highlighting the correct items.

## Other Goodies

### Glyhicons
The full set (commercial one) is in here

### Font-Awesome
All of the non commercials ones (v5) are in there as well

### Bootstrap 4
Already included

### Layout without a menu
If you need a layout without a menu, use the layout
```ruby
layout: :buybay_no_menu
```

### Stylesheet / CSS goodies
We have a few special class that you can use called 'buybaybg'
If you want a page with a dark background with the buybay stripes on it, apply this:
```ruby
<%= content_for :htmltag %>
  class="buybaybg"
<% end %>
```
Alternatively you can add id to the body tag:
```ruby
<%= content_for :bodytag %>
  class="buybaybg"
<% end %>
```



### yield
There is a yield in place in the layout, so regular output shall appear normally.
The following targets are also available:

* title: for page's title metatag
* description: for page's description metatag
* header_account: for username representation on the left side of the header
* header_details: for different needed details on the right side of the header
* javascript: Will place Javascript content in a script block in the header
* javascript_when_ready: Will place Javascript content in a windows.ready() function in the header
* bodytag, which is set in <body (here) >
* htmltag, which is set in <html (here) >

It works if there is a method "stored_content" defined in the application helper:
```ruby
  def stored_content
    content_for(:title) { 'BuyBay Vault' }
    content_for(:description) { 'BuyBay Vault: warehouse management system' }

    content_for(:header_account) { 'Welcome, ' + current_user[:first_name] } if current_user.present?

    if current_workstation.present?
      content_for(:header_details) { 'Workstation: ' + current_workstation.name }
    else
      content_for(:header_details) { 'No workstation is selected' }
    end
  end
```


### Logo generator

The BuyBay SVG logo generator is in there as a helper, you can use it by placing this in your code:
```ruby
   <%= buybay_logo(width:128,height:32,logo_color:"#0EA5B2") %>
```
The following parameters can be set:

* color: logo color
* height: The height of the logo
* width: The width of the logo
* include_letters: true or false to include or exclude the text 'BuyBay'
* include_palm_tree: true or false to include or exclude the palm tree logo
* opacity: opacity of the lines of the logo from 0 to 1
* fillopacity: opacity of the logo fills from 0 to 1
* id: DOM id

### Setting up a buybay table
Included in this gem there is a generic way of creating a buybay table.

First create a basic table html file, as the following example:

offer_items.html.slim

```ruby
.buybay-table-title
  | Offer items

.buybay-table-container
  table.buybay-table.cell-border.hover
    thead
      tr
        th Lorem ipsum
        th Lorem ipsum
        th Lorem ipsum
        th Lorem ipsum
        th Lorem ipsum
        th Lorem ipsum
    tbody
```
If you want to add a button to the datatable to '+ Add resource', then you must replace the .buybay-table-title class from the above example with the following code.

```ruby
.buybay-table-title-bar
  .buybay-table-title
    | Offer items
  = button_to new_contract_path, class: "btn btn-sm btn-warning", :method => :get, form: {target: '_blank'} do
    span.glyphicons.glyphicons-plus#plus-icon-inside-button
    | Add offer item
```

Then, create the javascript file that will create the table, as the following example:

```javascript
$(document).ready(function () {
  // Set the header list for your table. It is not what's going to be displayed
  // on the header, but it's an internal reference. It will have to match the
  // data coming from the json object, returned by the controller.

  var header_list = [
    "sku", "product_reference" ,"purchase_reference", "grade",
    "category-name", "created_at", "Online offers", "status"
  ]

  // Create a table identifier. It is used to generalize the table.

  table_identifier = {
    base_name: 'buybay',
    class_name: 'buybay-table',
    id: '.buybay-table'
  }

  // Setting a column to be sorted by default can be done as the following example:
  // In this example the third column will be sorted, in descending order.

  var order = [[2, 'desc']]

  // It's optional column properties. https://datatables.net/reference/option/columnDefs.targets
  // In the example below, column 3 will have class 'colored_column' added to it and
  // column 6 will not be sortable.

  var column_def = [{ className: "colored_column", "targets": 3 },
    { "orderable": false, "targets": 6 }]

  // Resource url where datatables will fetch the data for the table.
  // data needs to be structed as shown in the example below
  // https://datatables.net/examples/ajax/objects.html
  var url = base_url + "/all_stock_items"

  // Field with which the resource will be filterable.
  var filter_field = 'status'

  // Set a throttle frequency for searching.
  var delay = 400

  // Input a button to download the table as excel.
  var buttons = ['excel']

  // Initialize the table with the variables that are set.
  var table = initializeBuyBayTable(table_identifier, header_list, url, column_def, true, delay, [], buttons);

  // Option list for filtering, in this example it are the states.
  var filter_options = [
    'new', 'validating_content', 'content_error', 'cancelled',
    'returned_to_partner', 'stock_destroyed', 'awaiting_wms_export',
    'exported_to_wms', 'not_delivered', 'counted', 'sold_to_sti_traders',
    'disposed', 'in_exception', 'waiting_for_grading', 'in_grading',
    'waiting_repair', 'in_repair', 'preadvice_return', 'waiting_to_be_stored',
    'on_stock', 'published', 'to_be_picked', 'picked', 'shipped',
    'received_by_customer', 'missing', 'lost', 'warranty_claimed'
  ]

  // Url that is used on the on click event of a row from the table.
  var base_url = "/stock_items"
  $(table_identifier.id + ' tbody').on('click', 'tr', function(e){
    rowClick(table, base_url, this);
  });

  // Calls the buybayux gem with variables set above to enable filtering.
  addFilterData(filter_options, table_identifier, 'Select state');

  // Add popover with the table column header names.
  addPopOver(table.columns().header());

  // Add popover with the table column header names without filter input.
  addPopOverWithoutFilter(table.columns().header());

  // Add button with the calendar.
  addCalendarButton(table.columns().header());

  // Add button for export.
  addExportButton(table.columns().header());

  // Reload the table based on the selected or unselected field from the filter.
  $('.buybay-table-dropdown-options').on('select2:select select2:unselect', function (e) {
    reloadTableByFilterField(table, e, url, filter_field);
  });

  // Adds a listener to hide and show table header names based on the list from
  // the popover.
  document.addEventListener('click', function(event) {
    element = event.target
    if (element.tagName == 'A' &&
        element.classList.contains(table_identifier.class_name + '-toggle-vis')) {
          var column = table.column( $(element).attr('data-column') );
          column.visible( ! column.visible() );
    }
  });
});
```

### StatusBar element

StatusBar implements drawing of resource states. Module has predefined states and predefined list of states for specific resource. Basically, you have to define resource and current state for it. You are able redefine default states list in options.

#### Basic(minimal) usage:
```
HTML/SLIM:

div.statusbar data-current-state="online" data-resource="offer"

JS:

(new StatusBar()).draw();
```

Here StatusBar takes predefined states for "offer"(`['new', 'validate data', 'publishing online', 'online', 'not published', 'sold']`)

Predefined states for resources are listed in  `const ResourceStates` in `app/assets/javascripts/buybayux/statusbar.js`

#### HTML options:

`data-current-state` - defines current state, default 'new'

`data-resource` - defines resource(eg. offer, stock_item), optional

`data-state-alert` - display popover alert on top of state, optional


#### StatusBar options

`selector` - DOM selector, default: '.statusbar'

`currentState` - defines current state, same as html attribute `data-current-state`, has higher priority then html definition

`resource` - defines resource(eg. offer, stock_item), same as html attribute `data-resource`, has higher priority then html definition

`states` - list of states, redefine states for resource, obligatory if resource not predefined in StatusBar module

`statusAlert` - display popover alert on top of state, same as html attribute `data-state-alert`, has higher priority then html definition

`stateObjects` - extends predefined StateItems

#### StateItems(stateObjects)

For rendering each state module is using state objects with attributes:

`title` - required, title for state

`fa_icon` - required, icon to be displayed, for more icons check https://fontawesome.com/icon

`fallbackState` - optional, name of state that will be replaced. Some of states are replaced with another state. State with 'fallbackState' option are not rendered by default, if current state has 'fallbackState' option, current state will be rendered in place of 'fallbackState' value. eg. state "online" will be replaced with state "not published"

#### Example

```
HTML/SLIM:

div.stock-item-statusbar

JS:

var stackItemSB = new StatusBar({
  selector: '.stock-item-statusbar',
  currentState: 'sold',
  states: ['new', 'sold', 'online', 'custom state'],
  stateObjects: {
    'custom state': {
      title: 'This state is not predefined',
      fa_icon: 'fa-globe'
    }
  }
});
stackItemSB.draw();
```


### Add Buttons

addButtons function adds buttons for the selector. Button wrapped by 'action-btn-block' div class and have predefined self classes: 'btn', 'btn-sm' and 'd-inline-flex'. You can add custom icon and data attributes.

#### Basic usage:
```
JS:
var btnData = [
  {
    label: 'Add',
    icon: 'glyphicons glyphicons-plus',
    classes: ['btn', 'btn-sm', 'btn-warning'],
    dataAttributes: {
      'data-toggle': 'modal',
      'data-target': '#modal'
    }
  }
];

addButtons('.buybay-search-box', btnData);
```

`dataAttributes` - is optional property.
