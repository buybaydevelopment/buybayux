(function() {
  'use strict';

  window.StatusBar = function() {
    // default constructor
    this.options = null;
    this.element = null;
    this.states  = null;
    this.currentState = null;
    this.stateObjects = {};
    this.statusAlert = null;
    this.statesDatetime = {};

    var defaults = {
      selector: '.statusbar',
      currentState: 'new',
      resource: 'offer',
      states: [],
      stateObjects: {},
      statusAlert: '',
      statesDatetime: {}
    };

    if (arguments[0] && typeof arguments[0] === "object") {
      this.options = extendDefaults(defaults, arguments[0]);
    }

    var ResourceStates = {
      offer: ['new', 'validate data', 'publishing online', 'online', 'not published', 'sold'],
      stock_item: ['new', 'validate data', 'link service', 'allocate', 'validate stock', 'export to WMS', 'order shipped'],
      sales_order: ['new', 'allocate', 'validate stock', 'export to WMS', 'order shipped'],
    };

    var StateItems = {
      new: {
        title: 'New',
        fa_icon: 'fa-arrow-circle-right'
      },
      'validate data': {
        title: 'Validate Data',
        fa_icon: 'fa-link'
      },
      'link service': {
        title: 'Link Service Object',
        fa_icon: 'fa-link'
      },
      allocate: {
        title: 'Allocate',
        fa_icon: 'fa-dolly-flatbed'
      },
      'publishing online': {
        title: 'Publishing Online',
        fa_icon: 'fa-dolly-flatbed'
      },
      'validate stock': {
        title: 'Validate Stock',
        fa_icon: 'fa-boxes'
      },
      'export to WMS': {
        title: 'Export to WMS',
        fa_icon: 'fa-share'
      },
      'order shipped': {
        title: 'Order Shipped',
        fa_icon: 'fa-truck'
      },
      online: {
        title: 'Online',
        fa_icon: 'fa-globe'
      },
      'not published': {
        title: 'Not published',
        fa_icon: 'fa-exclamation',
        fallbackState: 'online'
      },
      sold: {
        title: 'Sold',
        fa_icon: 'fa-credit-card'
      },
    };

    this.init = function() {
      this.element  = $(this.options.selector);
      if (this.element.length == null) {
        console.log('Missed statusbar element: ' + s);
        return;
      }

      var resource      = this.element.data('resource') || this.options.resource
      this.states       = this.options.states.length ? this.options.states : ResourceStates[resource];
      this.currentState = this.element.data('current-state') || this.options.currentState;
      this.stateObjects = Object.assign({}, StateItems, this.options.stateObjects);
      this.statusAlert  = this.element.data('state-alert') || this.options.statusAlert;
      this.statesDatetime = this.element.data('states-datetime') || this.options.statesDatetime;

      console.log('StatusBar initialized');
      return this;
    }

    this.draw = function() {
      var element         = this.element;
      var stateObjects    = this.stateObjects;
      var statesDatetime  = this.statesDatetime;

      if (stateObjects[this.currentState] == null) {
        console.log('Unknown state: ' + this.currentState);
        return;
      }

      var currentFallback = stateObjects[this.currentState].fallbackState;
      var initialStates   = this.states.filter(function(s){
        return (stateObjects[s] == null) || (stateObjects[s].fallbackState == null);
      });

      // replace original state with fallback
      if (currentFallback != null) {
        var i = initialStates.indexOf(currentFallback);
        if (i !== -1) { initialStates[i] = this.currentState; }
      }

      initialStates.forEach(function (state) {
        var date_string = statesDatetime[state];
        var date;

        if (typeof date_string !== 'undefined') {
          date = parseDate(date_string);
        }

        element.append(renderState(state, stateObjects, date));
      });

      // add css styles
      var stateActive = $('[data-state="' + this.currentState + '"]');
      element.addClass('statusbar d-flex justify-content-center align-middle');
      stateActive.addClass('state-active');
      stateActive.nextAll().addClass('state-inactive');
      stateActive.prevAll().addClass('state-completed');

      // show alert message
      if (this.statusAlert.length > 0) {
        stateActive.attr('title', this.statusAlert);
        stateActive.tooltip({placement: 'top',trigger: 'manual', container: '.statusbar'}).tooltip('show');
        $(stateActive.data('bs.tooltip').tip).addClass('statusbar-tooltip');
      }

      return this;
    };

    function renderState(s, sObjects = StateItems, date) {
      var state = sObjects[s];
      var sClass = state.sClass || '';
      if (state == null) { console.log('Unknown state: ' + s); return; }
      if (state.fallbackState != null) { sClass += ' state-failed' }

      var span_block = '<span class="state flex-column ' + sClass + '" data-state="' + s + '">\
      <span class="rounded-circle">\
      <i class="fas ' + state.fa_icon + '"></i>\
      </span><span>' + state.title + '</span>';

      if(typeof date !== 'undefined') {
        span_block += '<div>' + date + '</div>';
      }

      return span_block + '</span>';
    };


    function extendDefaults(source, properties) {
      var property;
      for (property in properties) {
          if (properties.hasOwnProperty(property)) {
              source[property] = properties[property];
          }
      }
      return source;
    }

    this.init();
  }
}());
