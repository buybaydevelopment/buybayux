function setMenuEnvironmentColor(environment_name){
    switch (environment_name) {
        case 'production':
            var $mnu_color = 'menu_production';
            break;
        case 'demo':
            var $mnu_color = 'menu_demo';
            break;
        case 'staging':
            var $mnu_color = 'menu_staging';
            break;
        case 'development':
            var $mnu_color = 'menu_development';
            break;
        default:
            var $mnu_color = 'menu_production';
    }
    $('#mySideNav').removeClass('bg-sidebar').addClass($mnu_color);
}

$(window).on("load", function(){
    var activateSideBar = localStorage.getItem('activateSideBar');
    var checkSideBar = $('#check_side_menu_exists').val();

    if (checkSideBar == "true") {
        if (activateSideBar == 'short') {
            $("#mySideNav").width("50px");
            $("#main").css("marginLeft","50px");
            $("#messagebar").css("marginLeft","50px");
            $("#buybay_full_logo_column").css("display","none");
            $("#buybay_palm_only_logo_column").css("display","flex");
            $("#buybay_full_menu").css("display","none");
            $("#buybay_short_menu").css("display","block");
            localStorage.setItem('activateSideBar', 'short');
        } else {
            $("#mySideNav").width("250px");
            $("#main").css("marginLeft","250px");
            $("#buybay_full_logo_column").css("display","flex");
            $("#buybay_palm_only_logo_column").css("display","none");
            $("#buybay_full_menu").css("display","block");
            $("#buybay_short_menu").css("display","none");
            localStorage.setItem('activateSideBar', 'wide');
        }
    } else {
        $("#mySideNav").width("0px");
        $("#main").css("marginLeft","0px");
        $("#messagebar").css("marginLeft","0px");
        $("#buybay_full_logo_column").css("display","none");
        $("#buybay_palm_only_logo_column").css("display","none");
        $("#buybay_full_menu").css("display","none");
        $("#buybay_short_menu").css("display","none");
        localStorage.setItem('activateSideBar', 'none');
    }

    $("#myNavToggleButtonFull,#myNavToggleButtonShort").click(function (event) {
        event.preventDefault();

        var checkSideBar = $('#check_side_menu_exists').val();
      
        if (checkSideBar == "true") {
            if (event.currentTarget.id == "myNavToggleButtonFull")
            {
                $("#mySideNav").width("50px");
                $("#main").css("marginLeft","50px");
                $("#messagebar").css("marginLeft","50px");
                $("#buybay_full_logo_column").css("display","none");
                $("#buybay_palm_only_logo_column").css("display","flex");
                $("#buybay_full_menu").css("display","none");
                $("#buybay_short_menu").css("display","block");
                localStorage.setItem('activateSideBar', 'short');
            }
            else if (event.currentTarget.id == "myNavToggleButtonShort")
            {
                $("#mySideNav").width("250px");
                $("#main").css("marginLeft","250px");
                $("#messagebar").css("marginLeft","250px");
                $("#buybay_full_logo_column").css("display","flex");
                $("#buybay_palm_only_logo_column").css("display","none");
                $("#buybay_full_menu").css("display","block");
                $("#buybay_short_menu").css("display","none");
                localStorage.setItem('activateSideBar', 'wide');
            }
        } else {
            $("#mySideNav").width("0px");
            $("#main").css("marginLeft","0px");
            $("#messagebar").css("marginLeft","0px");
            $("#buybay_full_logo_column").css("display","none");
            $("#buybay_palm_only_logo_column").css("display","none");
            $("#buybay_full_menu").css("display","none");
            $("#buybay_short_menu").css("display","none");
            localStorage.setItem('activateSideBar', 'none');
        }
        return false;
    });
});
