function generateHeaderLinks(headers) {
  var headerLinks = headers.map(header => {
    var selectedClass = $(header).is(':visible') ? '' : 'unselect';
    if ($(header).hasClass('dt-col-hidden')) { selectedClass += ' d-none'; }

    return "<a class='buybay-table-toggle-vis " + selectedClass + "' data-column='" + headers.indexOf(header) + "'>\
            <span><i class='fa fa-check'></i></span>" + header.textContent + "</a>"
  });

  return headerLinks.join('');
};

function setColumnColors() {
  var column = $('.colored_column');
  for(i=0; i < column.length; i++) {
    if(column[i].innerText == 'A') {
      $(column[i]).addClass('a-grade')

    } else if(column[i].innerText == 'B') {
      $(column[i]).addClass('b-grade')

    } else if(column[i].innerText == 'C') {
      $(column[i]).addClass('c-grade')

    } else if(column[i].innerText == 'C-') {
      $(column[i]).addClass('c--grade')
    }
  }
};

function generateColumnHeaders(headers) {
  columns = []
  headers.forEach(function(header){
    columns.push({ "data": header });
  });
  return columns;
};

function rowClick(table, url, rowID) {
  var data = table.row( rowID ).data();
  var url = url + '/' + data['id'];
  window.open(url, '_blank');
};

function reloadTableByFilterField(table, event, url, filter_field) {
  filters = $(event.target).parent().find('[data-filter-key]');
  url = url + '?';

  filters.map(function(_i, item) {
    var $item = $(item);
    var filterKey = $item.attr('data-filter-key');
    var options = $item.find(':selected');

    var values = '';
    for (i = 0; i < options.length; i++) {
      if (i > 0) {
        values = values + ','
      }
      values = values + options[i].value
    };

    var filterParams = '';
    if (values.length > 0) {
      filterParams = 'filter[' + filterKey + ']=' + values;
    }

    if(filterParams.length > 1) {
      url = url + filterParams + '&';
    }
  });

  table.ajax.url( url )
  table.load();
  table.draw();
};

function addPopOver(headers) {
  $(function () {
    $('[data-toggle="popover"]').popover()
  })

  $(document).on('show.bs.popover', function() {
    $('.buybay-table-dropdown-filter').attr('data-content', generateHeaderLinks(headers));
  });

  var popOverAttributes = "<i tabindex='0' \
                        class='buybay-table-dropdown-filter fas fa-th fa-2x' \
                        data-container='body' \
                        data-html='true' \
                        data-content='' \
                        data-placement='bottom' \
                        data-toggle='popover' \
                        style='margin-left:15px; color: #262B33; \
                        padding: 5px; background-color: #BDE6EA; \
                        height: 32px; width: 32px'></i>"
  $('.buybay-table-dropdown').append(popOverAttributes);
  $('.buybay-table-dropdown-filter').attr('data-content', generateHeaderLinks(headers));
};

function hidePopOver() {
  $('body').on('click', function (e) {
    var popover = $('[data-toggle="popover"]');

    if (!$(popover).is(e.target) && $(popover).has(e.target).length === 0
      && $('.popover').has(e.target).length === 0) {
        $(popover).popover('hide');
    }
  });
};

function addFilterData(filter_options, table_identifier, placeholder) {
  $('.' + table_identifier.base_name + '-search-box').append("<div class='buybay-table-dropdown'><p>Filter:</p></div>");
  $('.buybay-table-dropdown').append("<div class='buybay-table-dropdown-options'></div>");
  $(".buybay-table-dropdown-options").select2({
    placeholder: placeholder,
    allowClear: true,
    multiple: true,
    width: '300px',
    debug: true,
    data: filter_options
  });
  $('.buybay-table-dropdown').find('.select2-selection--multiple').addClass('buybay-table-dropdown-grey');
};

function addFilters(filters_data, table_identifier) {
  var label = filters_data.length > 1 ? 'Filters:' : 'Filter';
  $('.' + table_identifier.base_name + '-search-box').append(`<div class='buybay-table-dropdown'><p>${label}</p></div>`);
  var dropdown = $('.buybay-table-dropdown');

  filters_data.map(function(filter, i) {

    dropdown.append("<div class='buybay-table-dropdown-options'></div>");
    if(filter.hasOwnProperty('ajax') && filter.ajax == 1) {
      dropdown.find('.buybay-table-dropdown-options:last').replaceWith("<select class='buybay-table-dropdown-options'></select>")
      $('.buybay-table-dropdown-options:last').select2({
        placeholder: filter.placeholder,
        minimumInputLength: 2,
        allowClear: true,
        multiple: true,
        width: filter.width,
        ajax: {
          url: filter.url,
          dataType: "json",
          type: "GET",
          data: function (params) {
            return {
              term: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data, params) {
            params.page = params.page || 1;

            return {
              results: data.results,
              pagination: {
                more: (params.page * 50) < data.total_count
              }
            };
          },

        }
      });
    } else {
      dropdown.find('.buybay-table-dropdown-options:last')
          .select2({
            placeholder: filter.placeholder,
            allowClear: true,
            multiple: true,
            width: filter.width,
            debug: true,
            data: filter.filter_options
          });

    }
    dropdown.find('.buybay-table-dropdown-options:last').attr('data-filter-key', filter.filter_identifier)
  });

  dropdown.find('.select2-selection--multiple').addClass('buybay-table-dropdown-grey');
};

function initializeBuyBayTable(table_identifier, header_list, url, column_def = [], domWithFilter = true, delay = 800, order = [[0, 'asc']], buttons = []){

  // default popover behaviour
  hidePopOver();

  // default DOM; filter excluded
  var domData = 'Brt<"bottom"<"page-counter">p><"clear">';
  var ajaxData = {"url": url, "type": "GET", 'headers': { "Accept": "application/json" }};
  var button_data = [
    {
      extend: 'excelHtml5',
      text: 'Excel',
      "exportOptions": {
        columns: ':visible'
      },
      customize: function( xlsx ) {
        var sheet = xlsx.xl.worksheets['sheet1.xml'];
        var alp = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')
        for (i=0; i < alp.length; i++) {
          $('row c[r^=' + alp[i] +']', sheet).attr( 's', '0' );
        }
      },
      action: function(e, dt, button, config) {
        this.processing(true)
        setTimeout(function () {
          $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(button), e, dt, button, config);
          dt.button(button).processing(false)
        },5);
      }
    }
  ]


  $(table_identifier.id).on('preInit.dt', function (e, settings, processing) {
    $('body').addClass('body-bg');
  });

  $(table_identifier.id).on('processing.dt', function (e, settings, processing) {
    if(processing) {
      $('body').addClass('body-bg');
    } else {
      $('body').removeClass('body-bg');
    }
  });

  var table = $(table_identifier.id).DataTable( {
    "ajax": ajaxData,
    "order": order,
    "columnDefs": column_def,
    "processing": true,
    "language":{
      "loadingRecords": "&nbsp;",
      "processing": "Loading..."
    },
    "serverSide": true,
    "columns": generateColumnHeaders(header_list),
    "iDisplayLength": 20,
    "dom": domWithFilter ? '<"top"f>' + domData : domData,
    "buttons": buttons.length > 0 ? button_data : buttons,
    "initComplete": function (e, settings, processing) {
      $('body').removeClass('body-bg');
    },
    "drawCallback": function( settings ) {
      $('.paginate_button.previous').html('<i class="fas fa-caret-left"></i>');
      $('.paginate_button.next').html('<i class="fas fa-caret-right"></i>');
      $('.paginate_button.current').addClass('buybay-table-pagination-grey-background');

      setColumnColors();

      //Show rows
      var totalRow = settings.json.recordsFiltered;
      var startRow = settings.oAjaxData.start + 1;
      var lengthRow = settings.oAjaxData.length - 1;
      var endRow = lengthRow + startRow;

      totalRow == 0 ? startRow = 0 : startRow;
      endRow > totalRow ? endRow = totalRow : endRow;

      $('.dataTables_info_custom').html('Showing ' + startRow + ' to ' + endRow + ' of ' + totalRow + ' entries</div>');
    }
  });

  // define custom page length block
  $('.page-counter').html('<p>Show:</p><div><select class="datatable-len"></select></div><p>results</p>');
  $('.datatable-len').select2({minimumResultsForSearch: Infinity, data: [20, 50, 75, 100, 5000]});
  $('.datatable-len').on('change', function () {
    table.page.len(this.value).draw();
  });

  $('.dataTable').next('.bottom').append('<div class="dataTables_info_custom"></div>');

  if (domWithFilter) {
    $(table_identifier.id + '_filter').attr('id', table_identifier.class_name + '_filter').addClass('pull-left');
    $(table_identifier.id + " thead tr th" ).removeClass( "colored_column" );
    $('.top').addClass(table_identifier.base_name + '-search-box');
    $(".dataTables_filter label").contents()[0].remove();
    $('.dataTables_filter').prepend("<span class='glyphicons glyphicons-search'></span>");
    $('input[type=search]').addClass('search-field');
    $('select[name=' + table_identifier.class_name + '_length]').addClass('entries-box');
  }

  $('.search-field')
      .unbind('keyup input')
      .bind('keypress', function (e) {
          if (e.which == 13) {
              var search = this.value;
              searchDelay = setTimeout(function() {
                  table.search(search).draw();
              }, delay);
          }
      }).css('width', '150px');
  return table;
};

function getFilterData(url, params = {}) {
  return $.ajax({
    'url': url,
    'data': params,
    'type': 'GET',
    'headers': { 'Accept': 'application/json' }
  });
};

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
};

function parseDate(string) {
  if(string) {
    parsedString = new Date(string);

    var date = parsedString.toLocaleDateString("en-GB").split('/').join('-');
    var hours = parsedString.getHours();
    var min = parsedString.getMinutes();

    return date + ' ' + formatNumber(hours) + ':' + formatNumber(min);
  } else {
    return '';
  }
};

function formatNumber(number) {
  return number < 10 ? '0' + number : number;
};

function addPopOverWithoutFilter(headers) {
  $(function () {
    $('[data-toggle="popover"]').popover()
  })

  $(document).on('show.bs.popover', function() {
    $('.buybay-table-dropdown-filter').attr('data-content', generateHeaderLinks(headers));
  });

  var popOverAttributes = "<i tabindex='0' \
                        class='buybay-table-dropdown-filter fas fa-th fa-2x' \
                        data-container='body' \
                        data-html='true' \
                        data-content='' \
                        data-placement='bottom' \
                        data-toggle='popover' \
                        style='margin-left:10px; color: #262B33; \
                        padding: 5px; background-color: #BDE6EA; \
                        height: 32px; width: 32px'></i>"
  $('.buybay-table-buttons').append(popOverAttributes);
  $('.buybay-table-dropdown-filter').attr('data-content', generateHeaderLinks(headers));
};

function addCalendarButton(header, table_identifier) {
  $('.' + table_identifier.base_name + '-search-box').append("<div class='buybay-table-buttons d-flex'></div>");
  var calendarAttributes = "<i tabindex='0' \
                        class='buybay-table-calendar fas fa-calendar-alt fa-2x' \
                        data-toggle='calendar' \
                        style='margin-left:10px; color: #262B33; \
                        padding: 5px; background-color: #BDE6EA; \
                        height: 32px; width: 32px'></i>"
  $('.buybay-table-buttons').append(calendarAttributes);
};

function addExportButton(headers) {
  var btnAttributes = "<div class='btn btn-info btn-sm ml-2'> \
                        <i class='fas fa-share mr-2' data-toggle='export'></i> \
                        <span> Export </span> \
                       </div>"
  $('.buybay-table-buttons').append(btnAttributes);
}

function addButtons(selection, btnNames = []) {
  if(btnNames.length == 0) {
    return;
  }

  var $actionBtnBlock = $('<div class = "action-btn-block mt-3"></div>');

  btnNames.map(function(item) {
    var $submitBtn = $('<button class="btn btn-sm d-inline-flex" type="submit"></button>');
    var attributes = item.dataAttributes;

    if(attributes) {
      setDataAttributes($submitBtn, attributes)
    }

    $submitBtn.addClass(item.classes.join(' '))
        .append('<i class="' + item.icon + '"></i>')
        .append(item.label);

    $actionBtnBlock.append($submitBtn);
  });

  $(selection).after($actionBtnBlock);
};

function setDataAttributes(button, attributes) {
  Object.keys(attributes).map(function(key) {
    button.attr(key, attributes[key]);
  });
}
