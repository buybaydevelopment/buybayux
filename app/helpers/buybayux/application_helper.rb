module Buybayux
  module ApplicationHelper
    # rubocop:disable Metrics/ParameterLists
    def buybay_logo(include_letters: true, include_palm_tree: true,
                    logo_color: BUYBAY_DARKCYAN, width: '100%',
                    height: '100%', opacity: 1, id: 'logo', fillopacity: 1)
      # returns an SVG logo

      render(partial: '/svg/logo.svg',
             locals: {
               color: logo_color,
               height: height,
               width: width,
               include_letters: include_letters,
               include_palm_tree: include_palm_tree,
               opacity: opacity,
               fillopacity: fillopacity,
               id: id
             })
    end
    # rubocop:enable Metrics/ParameterLists

    def sidebar_full_recursive(arra)
      ret = ''
      return ret if arra.nil?

      ret += '<div id="sidebar_accordion">'

      arra.each do |item|
        # toggle button
        ret += '<a class="sidenav-links collapsed" data-toggle="collapse" href="#' + item[:name].tr(' ', '_') + '"'
        ret += ' role="button" aria-expanded="false" aria-controls="' + item[:name].tr(' ', '_') + '">'
        ret += '<i class="bbcyan fa-fw ' + item[:icon] + '"></i>' unless item[:icon].nil?
        ret += '<span style="padding-left:20px;">' + item[:name] + '</span>'
        ret += '</a>'

        # submenu
        ret += '<div class="collapse" id="' + item[:name].tr(' ', '_') + '" data-parent="#sidebar_accordion">'
        item[:sub_menu].each do |subitem|
          next if subitem[:name].nil? || subitem[:url].nil?

          ret += '<a class="sidenav-links dropdown-item list-group-item menusub bbcyan submenubg" '
          ret += 'href="' + subitem[:url] + '">' + subitem[:name]
          ret += '</a>'
        end
        ret += '</div>'
      end

      ret += '</div>'
      ret
    end

    def sidebar_full
      struct = Buybayux.configuration.sidebar
      sidebar_full_recursive(struct).html_safe
    end

    def sidebar_short_builder(arra)
      ret = ''
      return ret if arra.nil?

      arra.each do |item|
        ret += '<div class="dropright">'
        # toggle button
        ret += '<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
        ret += '<i class="' + item[:icon] + ' bbcyan"></i>' unless item[:icon].nil?
        ret += '</a>'

        # submenu
        next if item[:sub_menu].count.zero?

        ret += '<div class="dropdown-menu bg-leftbottom">'
        item[:sub_menu].each do |subitem|
          next if subitem[:name].nil? || subitem[:url].nil?

          ret += '<a class="dropdown-item"'
          ret += 'href="' + subitem[:url] + '">' + subitem[:name]
          ret += '</a>'
        end
        ret += '</div>'

        # done
        ret += '</div>'
      end
      ret
    end

    def sidebar_short
      struct = Buybayux.configuration.sidebar
      sidebar_short_builder(struct).html_safe
    end
  end
end
