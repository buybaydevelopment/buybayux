module Buybayux
  class Configuration
    attr_accessor :sidebar
    attr_accessor :current_user
    attr_accessor :beta_version
  end

  class << self
    attr_writer :configuration
  end

  module_function

  def configuration
    @configuration ||= Configuration.new
  end

  def configure
    yield(configuration)
  end

  class Engine < ::Rails::Engine
    initializer :assets do
      Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf|otf)\z/
      Rails.application.config.assets.precompile += %w[buybayux/buybayux.css]
      Rails.application.config.assets.paths << File.expand_path('../../app/assets/stylesheets', __dir__)
      Rails.application.config.assets.paths << File.expand_path('../../app/assets/javascripts', __dir__)
      Rails.application.config.assets.paths << File.expand_path('../../app/assets/images', __dir__)
      Rails.application.config.assets.paths << File.expand_path('../../app/assets/fonts', __dir__)
      Rails.application.config.assets.paths << File.expand_path('../../app/assets/stylesheets/buybayux', __dir__)
      Rails.application.config.assets.paths << File.expand_path('../../app/assets/stylesheets/buybayux/fonts', __dir__)
      Rails.application.config.assets.precompile += %w[buybayux/buybayux.js]
      Rails.application.config.assets.paths << root.join('app', 'assets', 'fonts')
    end
  end
end
