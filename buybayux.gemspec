$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'buybayux/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'buybayux'
  s.version     = Buybayux::VERSION
  s.authors     = ['Roger Heykoop']
  s.email       = ['roger.heykoop@buybay.com']
  s.homepage    = 'https://www.buybay.com'
  s.summary     = 'Buybayux implements the front end design.'
  s.description = 'The Bootstrap 4 based theme for BuyBay frontends'
  s.license     = 'Private'
  s.add_dependency 'bootstrap', '~> 4.0.0'
  s.add_dependency 'google-tag-manager-rails', '~> 0.1.3'
  s.add_dependency 'jquery-rails'
  s.add_dependency 'rails', '>= 5.1'
  s.add_dependency 'rubocop'
  s.add_dependency 'sass-rails'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'sqlite3'
end
